#include "fsm.h"

//Implementasi state machine dengan software

//Variabel input
//input_button == 0 -> button ditekan
//input_battery == 0 -> baterai powerbank habis
//input_current == 0 -> tidak ada arus output dari powerbank

//Variabel output
//output_power == 0 -> LED untuk indikator power powerbank menyala 
//output_flashlight == 0 -> LED untuk indikator senter powerbank menyala
//state_top -> top-level-state untuk state machine yang digunakan
//state_low -> low-level-state untuk state machine yang digunakan

void fsm(int input_button, int input_battery, int input_current, int *output_power, 
		int *output_flashlight, int *state_top, int *state_low)
{
	static int counter_button = 0;
	static int counter_double = 0;
	static int counter_current = 0;

	switch(*state_top)
	{
		case BATTERY_OFF:
		{
			if(input_battery == 0)
				*state_top = BATTERY_OFF;

			else if (input_battery == 1)
			{
				*state_top = BATTERY_ON;
				*state_low = OFF_1; //Initial state untuk superstate BATTERY_ON
			}
			break;
		}

		case BATTERY_ON:
		{
			if(input_battery == 0)
				*state_top = BATTERY_OFF;

			else
			{
				*state_top = BATTERY_ON;

				switch(*state_low)
				{
					case OFF_1:
					{
						if(input_button == 0)
							*state_low = OFF_1;

						else if(input_button == 1)
							*state_low = OFF_2;
						break;
					}
					
					case OFF_2:
					{
						if(input_button == 1)
							*state_low = OFF_2;

						else if(input_button == 0)
							*state_low = ON_10;
						break;
					}
					
					case ON_1:
					{
						if(input_button == 0)
						{
							*state_low = ON_2;
							counter_button = 0;
						}

						else if(input_current == 0)
						{
							*state_low = ON_9;
							counter_current = 0;
						}

						else if(input_button == 1)
							*state_low = ON_1;
						break;
					}

					case ON_2:
					{
						if(input_button == 0)
						{
							*state_low = ON_2;
							counter_button++;
							if(counter_button >= LONG_PRESS_COUNT)
								*state_low = OFF_1;
						}
						else if(input_button == 1)
						{
							*state_low = ON_3;
							counter_double = 0;
						}
						break;
					}

					case ON_3:
					{
						if(input_button == 1)
						{
							*state_low = ON_3;
							counter_double++;
							if(counter_double >= DOUBLE_PRESS_COUNT)
								*state_low = ON_1;
						}
						else if(input_button == 0)
						{
							*state_low = ON_4;
							counter_button = 0;
						}
						break;
					}

					case ON_4:
					{
						if(input_button == 0)
						{
							*state_low = ON_4;
							counter_button++;
							if(counter_button >= LONG_PRESS_COUNT)
								*state_low = OFF_1;
						}
						else if(input_button == 1)
							*state_low = ON_5;
						break;
					}

					case ON_5:
					{
						if(input_button == 1)
							*state_low = ON_5;

						else if(input_button == 0)
						{
							*state_low = ON_6;
							counter_button = 0;
						}
						break;
					}

					case ON_6:
					{
						if(input_button == 0)
						{
							*state_low = ON_6;
							counter_button++;
							if(counter_button >= LONG_PRESS_COUNT)
								*state_low = OFF_1;
						}

						else if(input_button == 1)
						{
							*state_low = ON_7;
							counter_double = 0;
						}
						break;
					}

					case ON_7:
					{
						if(input_button == 1)
						{
							*state_low = ON_7;
							counter_double++;
							if(counter_double >= DOUBLE_PRESS_COUNT)
								*state_low = ON_5;
						}

						else if(input_button == 0)
						{
							*state_low = ON_8;
							counter_button = 0;
						}
						break;
					}

					case ON_8:
					{
						if(input_button == 0)
						{
							*state_low = ON_8;
							counter_button++;
							if(counter_button >= LONG_PRESS_COUNT)
								*state_low = OFF_1;
						}

						else if(input_button == 1)
							*state_low = ON_1;
						break;
					}

					case ON_9:
					{
						if(input_button == 0)
						{
							*state_low = ON_2;
							counter_button = 0;
						}

						else if(input_current == 0)
						{
							*state_low = ON_9;
							counter_current++;
							if(counter_current >= ACTIVITY_TIMEOUT_COUNT)
								*state_low = OFF_1;
						}

						else if(input_current == 1)
							*state_low = ON_1;

						break;
					}

					case ON_10:
					{
						if(input_button == 0)
							*state_low = ON_10;

						else if(input_button == 1)
							*state_low = ON_1;

						break;
					}

					default:
					{

					}
				}
			}
			break;
		}

		default:
		{

		}
	}

	//Perhitungan output
	switch(*state_top)
	{
		case BATTERY_OFF:
		{
			*output_power = 1;
			*output_flashlight = 1;
			break;
		}

		case BATTERY_ON:
		{
			switch(*state_low)
			{
				case OFF_1:		
				case OFF_2:
				{
					*output_power = 1;
					*output_flashlight = 1;
					break;
				}
				case ON_1:
				case ON_2:
				case ON_3:
				{
					*output_power = 0;
					*output_flashlight = 1;
					break;
				}
				case ON_4:
				case ON_5:
				case ON_6:
				case ON_7:
				{
					*output_power = 0;
					*output_flashlight = 0;
					break;
				}
				case ON_8:
				case ON_9:
				case ON_10:
				{
					*output_power = 0;
					*output_flashlight = 1;
					break;
				}
				default:
				{

				}
			}
			break;
		}
		default:
		{

		}
	}
}