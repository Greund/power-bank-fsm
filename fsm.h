#ifndef FSM_H
#define FSM_H

#define BATTERY_OFF 0
#define BATTERY_ON 1

#define OFF_1 0
#define OFF_2 1
#define ON_1 2
#define ON_2 3
#define ON_3 4
#define ON_4 5
#define ON_5 6
#define ON_6 7
#define ON_7 8
#define ON_8 9
#define ON_9 10
#define ON_10 11

#define LONG_PRESS_COUNT 100
#define DOUBLE_PRESS_COUNT 50
#define ACTIVITY_TIMEOUT_COUNT 3000

void fsm(int input_button, int input_battery, int input_current, int *output_power, 
		int *output_flashlight, int *state_top, int *state_low);

#endif	/* FSM_H */