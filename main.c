/*****************************************************
Chip type               : ATmega328P
Program type            : Application
AVR Core Clock frequency: 16.000000 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 512
*****************************************************/

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include "fsm.h"
#include <util/delay.h>

int state_top = BATTERY_OFF;
int state_low;

//Fungsi untuk menerima karakter dari USART
unsigned char USART_Receive(void)
{
	//Menunggu ada data di buffer
	while ( !(UCSR0A & (1<<RXC0)) )
	;
	//Terima karakter dari buffer dan return nilainya
	return UDR0;
}

//Fungsi untuk mengirim karakter ke USART
void USART_Transmit(unsigned char data)
{
	//Menunggu buffer kosong
	while ( !( UCSR0A & (1<<UDRE0)) )
	;
	//Masukan data yang akan dikirimkan ke buffer
	UDR0 = data;
}

//Fungsi untuk mengirim string ke USART
void USART_Transmit_String(char *data)
{
	unsigned char* p_toSend;

	p_toSend = (unsigned char*)data;
	while(*p_toSend)
	{
		USART_Transmit(*p_toSend);
		p_toSend++;
	}
}

// Timer1 output compare A interrupt service routine
ISR(TIMER1_COMPA_vect)
{
	int pin_input;
	int input_button, input_battery, input_current;
	int output_power, output_flashlight;
	
	//Input
	pin_input = PIND; //Input di PD2, PD3, PD4
	//PD2 untuk input button
	if(~pin_input & (1 << 2))
		input_button = 0;
	else
		input_button = 1;

	//PD3 untuk input toggle switch baterai
	if(~pin_input & (1 << 3))
		input_battery = 0;
	else
		input_battery = 1;

	//PD4 untuk input toggle switch arus
	if(~pin_input & (1 << 4))
		input_current = 0;
	else
		input_current = 1;

	//Fungsi FSM
	fsm(input_button, input_battery, input_current, &output_power, &output_flashlight, &state_top, &state_low);

	//Output
	//Output di PD5, PD6
	//PD5 untuk output power powerbank
	if (output_power == 0)
		PORTD &= ~(1 << 5);
	else
		PORTD |= 1 << 5;

	//PD6 untuk output senter powerbank
	if (output_flashlight == 0)
		PORTD &= ~(1 << 6);
	else
		PORTD |= 1 << 6;
}

int main(void)
{
	// Crystal Oscillator division factor: 1
	CLKPR = 0x80;
	CLKPR = 0x00;

	// Input/Output Ports initialization
	// Enable pull-up
	MCUCR &= ~(1 < PUD);

	// Port B initialization
	// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
	// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
	DDRB = 0x00;
	PORTB = 0x00;

	// Port C initialization
	// Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In 
	// State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
	DDRC = 0x00;
	PORTC = 0x00;

	// Port D initialization
	// Func7=In Func6=Out Func5=Out Func4=In Func3=In Func2=In Func1=In Func0=In 
	// State7=T State6= State5= State4=pull-up State3=pull-up State2=pull-up State1=T State0=T 
	DDRD = (1 << 5) | (1 << 6); //PD5 dan PD6 sebagai output
	PORTD = (1 << 2) | (1 << 3) | (1 << 4); //PD2, PD3, dan PD4 di pull-up


	// Timer/Counter 0 initialization
	// Clock source: System Clock
	// Clock value: Timer 0 Stopped
	// Mode: Normal top=0xFF
	// OC0A output: Disconnected
	// OC0B output: Disconnected
	TCCR0A = 0x00;
	TCCR0B = 0x00;
	TCNT0 = 0x00;
	OCR0A = 0x00;
	OCR0B = 0x00;

	// Timer/Counter 1 initialization
	// Clock source: System Clock
	// Clock value: 62.500 kHz
	// Mode: CTC top=OCR1A
	// OC1A output: Discon.
	// OC1B output: Discon.
	// Noise Canceler: Off
	// Input Capture on Falling Edge
	// Timer1 Overflow Interrupt: Off
	// Input Capture Interrupt: Off
	// Compare A Match Interrupt: On
	// Compare B Match Interrupt: Off
	TCCR1A = 0x00;
	TCCR1B = 0x0C;
	TCNT1H = 0x00;
	TCNT1L = 0x00;
	ICR1H = 0x00;
	ICR1L = 0x00;

	OCR1AH = 0x02; //0x0271 = 625 (100 Hz)
	OCR1AL = 0x71;

	OCR1BH = 0x00;
	OCR1BL = 0x00;

	// Timer/Counter 2 initialization
	// Clock source: System Clock
	// Clock value: Timer2 Stopped
	// Mode: Normal top=0xFF
	// OC2A output: Disconnected
	// OC2B output: Disconnected
	ASSR = 0x00;
	TCCR2A = 0x00;
	TCCR2B = 0x00;
	TCNT2 = 0x00;
	OCR2A = 0x00;
	OCR2B = 0x00;

	// External Interrupt(s) initialization
	// INT0: Off
	// INT1: Off
	// Interrupt on any change on pins PCINT0-7: Off
	// Interrupt on any change on pins PCINT8-14: Off
	// Interrupt on any change on pins PCINT16-23: Off
	EICRA = 0x00;
	EIMSK = 0x00;
	PCICR = 0x00;

	// Timer/Counter 0 Interrupt(s) initialization
	TIMSK0 = 0x00;

	// Timer/Counter 1 Interrupt(s) initialization
	TIMSK1 = 0x02;

	// Timer/Counter 2 Interrupt(s) initialization
	TIMSK2 = 0x00;

	// USART initialization
	// Enabling TX & RX
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	UCSR0A = (1<<UDRE0);
	UCSR0C =  (1 << UCSZ01) | (1 << UCSZ00); //Set frame: 8data, 1 stop
	UBRR0H = 0x00;
	UBRR0L = 0x67;

	// Analog Comparator initialization
	// Analog Comparator: Off
	// Analog Comparator Input Capture by Timer/Counter 1: Off
	ACSR = 0x80;
	ADCSRB = 0x00;
	DIDR1 = 0x00;

	// ADC initialization
	// ADC disabled
	ADCSRA = 0x00;

	// SPI initialization
	// SPI disabled
	SPCR = 0x00;

	// TWI initialization
	// TWI disabled
	TWCR = 0x00;

	// Global enable interrupts
	sei();

	while (1)
	{
		//Serial debugging
		USART_Transmit_String("Top state: ");
		switch(state_top)
		{
			case BATTERY_OFF:
				USART_Transmit_String("BATTERY_OFF");
				break;

			case BATTERY_ON:
				USART_Transmit_String("BATTERY_ON");
				break;
		}
		USART_Transmit_String("\t");
		USART_Transmit_String("Low state: ");
		switch(state_low)
		{
			case OFF_1:
				USART_Transmit_String("OFF_1");
				break;

			case OFF_2:
				USART_Transmit_String("OFF_2");
				break;

			case ON_1:
				USART_Transmit_String("ON_1");
				break;

			case ON_2:
				USART_Transmit_String("ON_2");
				break;

			case ON_3:
				USART_Transmit_String("ON_3");
				break;

			case ON_4:
				USART_Transmit_String("ON_4");
				break;

			case ON_5:
				USART_Transmit_String("ON_5");
				break;

			case ON_6:
				USART_Transmit_String("ON_6");
				break;

			case ON_7:
				USART_Transmit_String("ON_7");
				break;

			case ON_8:
				USART_Transmit_String("ON_8");
				break;

			case ON_9:
				USART_Transmit_String("ON_9");
				break;

			case ON_10:
				USART_Transmit_String("ON_10");
				break;
		}
		USART_Transmit_String("\n");
		_delay_ms(1000);
	}
}